package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type GitLabClient struct {
	httpCli *http.Client
	token   string
	baseURL string
	dryRun  bool
}

func (g *GitLabClient) getPath(path string) string {
	return fmt.Sprintf("%s/%s?private_token=%s&per_page=1000", g.baseURL, path, g.token)
}

func NewGitLabClient(token, baseURL string, insecure bool, dryRun bool) *GitLabClient {
	baseURL = strings.TrimSuffix(baseURL, "/")
	httpCli := http.DefaultClient
	httpCli.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}
	httpCli.Transport = &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: insecure,
		},
	}
	return &GitLabClient{
		token:   token,
		baseURL: baseURL,
		httpCli: httpCli,
		dryRun:  dryRun,
	}
}

func (g *GitLabClient) doRequest(method, path string, body interface{}) (*http.Response, error) {
	var buf io.Reader
	if body != nil {
		b, err := json.Marshal(body)
		if err != nil {
			return nil, err
		}
		buf = bytes.NewBuffer(b)
	}
	req, err := http.NewRequest(method, g.getPath(path), buf)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	req.Close = true
	return g.httpCli.Do(req)
}

func (g *GitLabClient) doFormRequest(method, path string, values map[string]string) (*http.Response, error) {
	form := url.Values{}
	for key, val := range values {
		form.Add(key, val)
	}
	req, err := http.NewRequest(method, g.getPath(path), strings.NewReader(form.Encode()))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "multipart/form-data")
	req.PostForm = form
	return g.httpCli.Do(req)
}

func (g *GitLabClient) GetGroupProjects(group int) ([]*Project, error) {
	resp, err := g.doRequest("GET", fmt.Sprintf("groups/%d/projects", group), nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	projects := []*Project{}
	if err := json.Unmarshal(bytes, &projects); err != nil {
		return nil, err
	}
	return projects, nil
}

// GetProjectVariables – Get list of a project's variables.
func (g *GitLabClient) GetProjectVariables(project int) ([]*Variable, error) {
	return g.getVariables("projects", project)
}

// GetGroupVariables – Get list of a groups's variables.
func (g *GitLabClient) GetGroupVariables(group int) ([]*Variable, error) {
	return g.getVariables("groups", group)
}

func (g *GitLabClient) getVariables(ns string, entry int) ([]*Variable, error) {
	resp, err := g.doRequest("GET", fmt.Sprintf("%s/%d/variables", ns, entry), nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	variables := []*Variable{}
	if err := json.Unmarshal(bytes, &variables); err != nil {
		return nil, err
	}
	return variables, nil
}

// CreateProjectVariable – Create a new variable
func (g *GitLabClient) CreateProjectVariable(project int, key, value string) error {
	if g.dryRun {
		return nil
	}
	return g.createVariable("projects", project, key, value)
}

// CreateGroupVariable – Create a new variable
func (g *GitLabClient) CreateGroupVariable(group int, key, value string) error {
	if g.dryRun {
		return nil
	}
	return g.createVariable("groups", group, key, value)
}

func (g *GitLabClient) createVariable(ns string, entry int, key, value string) error {
	variable := map[string]string{
		"key":   key,
		"value": value,
	}
	resp, err := g.doFormRequest("POST", fmt.Sprintf("%s/%d/variables", ns, entry), variable)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("Unexpected response code: %s", resp.Status)
	}
	return nil
}

// UpdateProjectVariable – Update a project's variable
func (g *GitLabClient) UpdateProjectVariable(project int, key, value string) error {
	if g.dryRun {
		return nil
	}
	return g.updatеVariable("projects", project, key, value)
}

// UpdateGroupVariable – Update a group's variable
func (g *GitLabClient) UpdateGroupVariable(group int, key, value string) error {
	if g.dryRun {
		return nil
	}
	return g.updatеVariable("groups", group, key, value)
}

func (g *GitLabClient) updatеVariable(ns string, entry int, key, value string) error {
	variable := map[string]string{
		"value": value,
	}
	resp, err := g.doFormRequest("PUT", fmt.Sprintf("%s/%d/variables/%s", ns, entry, key), variable)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Unexpected response code: %s", resp.Status)
	}
	return nil
}

// DeleteProjectVariable – Remove a project's variable
func (g *GitLabClient) DeleteProjectVariable(project int, key string) error {
	if g.dryRun {
		return nil
	}
	return g.deleteVariable("projects", project, key)
}

// DeleteGroupVariable – Remove a groups's variable
func (g *GitLabClient) DeleteGroupVariable(group int, key string) error {
	if g.dryRun {
		return nil
	}
	return g.deleteVariable("groups", group, key)
}

func (g *GitLabClient) deleteVariable(ns string, entry int, key string) error {
	resp, err := g.doRequest("DELETE", fmt.Sprintf("%s/%d/variables/%s", ns, entry, key), nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("Unexpected response code: %s", resp.Status)
	}
	return nil
}
