package main

import (
	"fmt"
	"strings"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Projects []*ConfigProject `yaml:"projects,omitempty"`
	Group    *ConfigGroup     `yaml:"group,omitempty"`
}

type ConfigProject struct {
	Name string      `yaml:"name"`
	Env  []*Variable `yaml:"env"`
}

type ConfigGroup struct {
	ID  int         `yaml:"id"`
	Env []*Variable `yaml:"env"`
}

type Project struct {
	ID                   int        `json:"id"`
	Description          string     `json:"description"`
	Name                 string     `json:"name"`
	DefaultBranch        string     `json:"default_branch"`
	IssuesEnabled        bool       `json:"issues_enabled"`
	MergeRequestsEnabled bool       `json:"merge_requests_enabled"`
	WikiEnabled          bool       `json:"wiki_enabled"`
	JobsEnabled          bool       `json:"jobs_enabled"`
	SnippetsEnabled      bool       `json:"snippets_enabled"`
	SharedRunnersEnabled bool       `json:"shared_runners_enabled"`
	Namespace            *Namespace `json:"Namespace"`
	ImportStatus         string     `json:"import_status"`
	OpenIssuesCount      int        `json:"open_issues_count"`
}

type Namespace struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type Variable struct {
	Key              string `json:"key" yaml:"name,omitempty"`
	Value            string `json:"value" yaml:"value,omitempty"`
	Protected        bool   `json:"protected" yaml:"protected,omitempty"`
	EnvironmentScope string `json:"environment_scope" yaml:"environmentScope,omitempty"`
}

func (c *Config) String() string {
	var str strings.Builder

	str.WriteString(fmt.Sprintf("Group ID: %d\n", c.Group.ID))
	if len(c.Group.Env) > 0 {
		str.WriteString("Variable:")
		for _, v := range c.Group.Env {
			str.WriteString(fmt.Sprintf("%s=%s\n", v.Key, v.Value))
		}
	}
	for _, p := range c.Projects {
		str.WriteString(fmt.Sprintf("Project name: %s\n", p.Name))
		if len(p.Env) > 0 {
			str.WriteString("Variable:")
			for _, v := range p.Env {
				str.WriteString(fmt.Sprintf("%s=%s\n", v.Key, v.Value))
			}
		}
	}

	return str.String()
}

func (c *Config) YAML() string {
	out, _ := yaml.Marshal(c)
	return string(out)
}
