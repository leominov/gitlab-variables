# GitLab variables

## Configuration

```
---
group:
  id: 10
  env:
    - name: CLOUD_SDK_VERSION
      value: 206.0.0
    - name: DB_HOST
      value: 127.0.0.1
    - name: DB_PORT
      value: 5432
projects:
  - name: service1
    env:
      - name: LISTEN_PORT
        value: 3000
      - name: DB_USER
        value: service1_accout
      - name: DB_PASSWORD
        value: quee0oNia3Pee9Mohz9uthahLa4eghah
  - name: service2
    env:
      - name: LISTEN_PORT
        value: 3001
      - name: DB_USER
        value: service2_accout
      - name: DB_PASSWORD
        value: eipeit4ohM6yah0chaijoyahhie5ieC4
  - name: service3
    env:
      - name: LISTEN_PORT
        value: 3002
      - name: DB_USER
        value: service3_accout
      - name: DB_PASSWORD
        value: thieg9aejeidahdohTheexio0OG7yohp
```

## Usage

```
  -api string
    	GitLab API URL (default "https://gitlab.local/api/v4")
  -config string
    	Configuration file (default "config.yml")
  -dry-run
    	Do not create, delete or update anything (default true)
  -group
    	Update group's variables (default true)
  -insecure
    	Allow insecure API calls
  -list
    	Prints list of a project's and group's variables
  -list-format string
    	Format for variables list (text, yaml) (default "text")
  -project
    	Update project's variables (default true)
  -token string
    	Personal GitLab token
```
