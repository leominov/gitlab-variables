package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/fatih/color"
	"gopkg.in/yaml.v2"
)

var (
	PersonalTokenFlag  = flag.String("token", "", "Personal GitLab token")
	ConfigFileFlag     = flag.String("config", "config.yml", "Configuration file")
	GitLabAPIFlag      = flag.String("api", "https://gitlab.local/api/v4", "GitLab API URL")
	InsecureFlag       = flag.Bool("insecure", false, "Allow insecure API calls")
	DryRunFlag         = flag.Bool("dry-run", true, "Do not create, delete or update anything")
	UpdateProjectsFlag = flag.Bool("project", true, "Update project's variables")
	UpdateGroupFlag    = flag.Bool("group", true, "Update group's variables")
	ListFlag           = flag.Bool("list", false, "Prints list of a project's and group's variables")
	ListFormat         = flag.String("list-format", "text", "Format for variables list (text, yaml)")

	config *Config
	client *GitLabClient

	title = color.New(color.FgCyan).Add(color.Underline)
	done  = color.New(color.FgGreen)
)

func hasVariable(vars []*Variable, key string) (*Variable, bool) {
	for _, variable := range vars {
		if variable.Key == key {
			return variable, true
		}
	}
	return nil, false
}

func projectID(projects []*Project, name string) int {
	for _, project := range projects {
		if project.Name == name {
			return project.ID
		}
	}
	return 0
}

func updateProjects() ([]*ConfigProject, error) {
	var projectConfigs []*ConfigProject
	projects, err := client.GetGroupProjects(config.Group.ID)
	if err != nil {
		return nil, err
	}
	for _, project := range config.Projects {
		title.Println(project.Name)
		projectID := projectID(projects, project.Name)

		currentVars, err := client.GetProjectVariables(projectID)
		if err != nil {
			fmt.Printf("Error getting '%s' project's variables: %v\n", project.Name, err)
			continue
		}

		projectConfig := &ConfigProject{
			Name: project.Name,
		}
		for _, variable := range currentVars {
			projectConfig.Env = append(projectConfig.Env, &Variable{
				Key:   variable.Key,
				Value: variable.Value,
			})
		}
		projectConfigs = append(projectConfigs, projectConfig)

		for _, variable := range project.Env {
			if v, ok := hasVariable(currentVars, variable.Key); ok {
				if variable.Value == v.Value {
					continue
				}
				fmt.Printf("🔄 Updating '%s' variable from '%s' to '%s'...\n", variable.Key, v.Value, variable.Value)
				err := client.UpdateProjectVariable(projectID, variable.Key, variable.Value)
				if err != nil {
					fmt.Printf("Error updating '%s' variable: %v\n", variable.Key, err)
				}
				continue
			}
			fmt.Printf("✅ Adding '%s' variable with value '%s'...\n", variable.Key, variable.Value)
			err := client.CreateProjectVariable(projectID, variable.Key, variable.Value)
			if err != nil {
				fmt.Printf("Error creating '%s' variable: %v\n", variable.Key, err)
			}
		}

		for _, variable := range currentVars {
			if _, ok := hasVariable(project.Env, variable.Key); ok {
				continue
			}
			fmt.Printf("❌ Removing '%s' variable with value '%s'...\n", variable.Key, variable.Value)
			err := client.DeleteProjectVariable(projectID, variable.Key)
			if err != nil {
				fmt.Printf("Error removing '%s' variable: %v\n", variable.Key, err)
			}
		}

		fmt.Println()
	}

	return projectConfigs, nil
}

func updateGroup() (*ConfigGroup, error) {
	title.Printf("group #%d\n", config.Group.ID)

	currentVars, err := client.GetGroupVariables(config.Group.ID)
	if err != nil {
		return nil, fmt.Errorf("error getting group's variables: %v", err)
	}

	groupConfig := &ConfigGroup{
		ID: config.Group.ID,
	}

	for _, variable := range currentVars {
		groupConfig.Env = append(groupConfig.Env, &Variable{
			Key:   variable.Key,
			Value: variable.Value,
		})
	}

	for _, variable := range config.Group.Env {
		if v, ok := hasVariable(currentVars, variable.Key); ok {
			if variable.Value == v.Value {
				continue
			}
			fmt.Printf("🔄 Updating '%s' group's variable from '%s' to '%s'...\n", variable.Key, v.Value, variable.Value)
			err := client.UpdateGroupVariable(config.Group.ID, variable.Key, variable.Value)
			if err != nil {
				fmt.Printf("Error updating '%s' group's variable: %v\n", variable.Key, err)
			}
			continue
		}
		fmt.Printf("✅ Adding '%s' group's variable with value '%s'...\n", variable.Key, variable.Value)
		err := client.CreateGroupVariable(config.Group.ID, variable.Key, variable.Value)
		if err != nil {
			fmt.Printf("Error creating '%s' group's variable: %v\n", variable.Key, err)
		}
	}

	for _, variable := range currentVars {
		if _, ok := hasVariable(config.Group.Env, variable.Key); ok {
			continue
		}
		fmt.Printf("❌ Removing '%s' group's variable...\n", variable.Key)
		err := client.DeleteGroupVariable(config.Group.ID, variable.Key)
		if err != nil {
			fmt.Printf("Error removing '%s' group's variable: %v\n", variable.Key, err)
		}
	}

	return groupConfig, nil
}

func main() {
	flag.Parse()

	if len(*PersonalTokenFlag) == 0 {
		fmt.Println("Personal token must be specified")
		os.Exit(1)
	}

	body, err := ioutil.ReadFile(*ConfigFileFlag)
	if err != nil {
		fmt.Printf("Error loading configuration file: %v\n", err)
		os.Exit(1)
	}

	config = &Config{}
	if err := yaml.Unmarshal(body, config); err != nil {
		fmt.Printf("Error processing configuration: %v\n", err)
		os.Exit(1)
	}

	client = NewGitLabClient(*PersonalTokenFlag, *GitLabAPIFlag, *InsecureFlag, *DryRunFlag)

	config := &Config{}

	if *UpdateProjectsFlag {
		projects, err := updateProjects()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		config.Projects = projects
	}

	if *UpdateGroupFlag {
		group, err := updateGroup()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		config.Group = group
	}

	if *ListFlag {
		if *ListFormat == "yaml" {
			fmt.Println(config.YAML())
			return
		}
		fmt.Println(config.String())
		return
	}

	done.Println("done")
}
